/* window.vala
 *
 * Copyright 2020 Felix Häcker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Sidebarwidget {
    [GtkTemplate (ui = "/de/haeckerfelix/SidebarWidget/window.ui")]
    public class Window : Hdy.ApplicationWindow {

        [GtkChild]
        Gtk.Box flap_widget;
        [GtkChild]
        Gtk.Box content_widget;
        [GtkChild]
        Gtk.Box flap_box;

        [GtkChild]
        Hdy.HeaderBar flap_header;
        [GtkChild]
        Hdy.HeaderBar content_header;

        [GtkChild]
        Gtk.Switch reveal_flap_switch;
        [GtkChild]
        Gtk.Switch locked_switch;
        [GtkChild]
        Gtk.CheckButton folded_checkbutton;

        [GtkChild]
        Gtk.ToggleButton start_toggle_button;
        [GtkChild]
        Gtk.ToggleButton end_toggle_button;

        [GtkChild]
        Gtk.ToggleButton vertical_toggle_button;
        [GtkChild]
        Gtk.ToggleButton horizontal_toggle_button;

        Hdy.Flap flap;
        Hdy.HeaderGroup hdygroup;

        public Window (Gtk.Application app) {
            Object (application: app);

            // Load custom styling
            var p = new Gtk.CssProvider();
            p.load_from_resource("/de/haeckerfelix/SidebarWidget/style.css");
            Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), p, 500);

            flap = new Hdy.Flap();
            flap.expand = true;

            flap.add(content_widget);
            flap.flap = flap_widget;

            hdygroup = new Hdy.HeaderGroup();

            refresh_header_group ();
            flap.notify["flap-position"].connect (refresh_header_group);

            reveal_flap_switch.bind_property("state", flap, "reveal-flap", BindingFlags.BIDIRECTIONAL);
            locked_switch.bind_property("state", flap, "locked", BindingFlags.BIDIRECTIONAL);
            flap.bind_property("folded", folded_checkbutton, "active", BindingFlags.DEFAULT);

            start_toggle_button.bind_property("active", end_toggle_button, "active", BindingFlags.INVERT_BOOLEAN);
            end_toggle_button.bind_property("active", start_toggle_button, "active", BindingFlags.INVERT_BOOLEAN);

            vertical_toggle_button.bind_property("active", horizontal_toggle_button, "active", BindingFlags.INVERT_BOOLEAN);
            horizontal_toggle_button.bind_property("active", vertical_toggle_button, "active", BindingFlags.INVERT_BOOLEAN);

            flap_box.add(flap);
            this.show_all();
        }

        [GtkCallback]
        private void start_toggle_button_toggled(Gtk.ToggleButton button){
            if (button.active){
                flap.flap_position = Gtk.PackType.START;
            }else{
                flap.flap_position = Gtk.PackType.END;
            }
        }

        [GtkCallback]
        private void vertical_toggle_button_toggled(Gtk.ToggleButton button){
            if (button.active){
                flap.orientation = Gtk.Orientation.VERTICAL;
            }else{
                flap.orientation = Gtk.Orientation.HORIZONTAL;
            }
        }

        [GtkCallback]
        private void flap_mode_combobox_changed(Gtk.ComboBox cbt){
            switch (cbt.get_active_id()){
                case "never": flap.flap_fold_policy = Hdy.FlapFoldPolicy.NEVER; break;
                case "always": flap.flap_fold_policy = Hdy.FlapFoldPolicy.ALWAYS; break;
                case "auto": flap.flap_fold_policy = Hdy.FlapFoldPolicy.AUTO; break;
            }
        }

        private void refresh_header_group () {
            hdygroup.remove_header_bar (content_header);
            hdygroup.remove_header_bar (flap_header);

            switch (flap.flap_position) {
            case Gtk.PackType.START:
                hdygroup.add_header_bar (flap_header);
                hdygroup.add_header_bar (content_header);
                break;

            case Gtk.PackType.END:
                hdygroup.add_header_bar (content_header);
                hdygroup.add_header_bar (flap_header);
                break;

            default:
                assert_not_reached ();
            }
        }
    }
}
